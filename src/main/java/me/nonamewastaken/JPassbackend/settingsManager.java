package me.nonamewastaken.JPassbackend;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

public class settingsManager {

    public Properties rp;
    public Properties wp;

    public void readSettings() throws IOException { //has to be run on every startup
        rp = new Properties();
        InputStream i = new FileInputStream("server.properties");
        rp.load(i);
    }

    public void setup(){
        wp = new Properties();
        OutputStream os;
        Scanner scnr = new Scanner(System.in);
        try {
            os = new FileOutputStream("server.properties");
            clearConsole();
            System.out.println("ALL PROVIDED DATA WILL BE STORED IN PLAINTEXT\n");
            System.out.println("Your smtp host (IP/domain name):");
            wp.setProperty("smtpHost", scnr.nextLine());
            System.out.println("The smtp port of given server:");
            wp.setProperty("smtpPort", scnr.nextLine());
            System.out.println("do you want to use smtp credentials (type true/false):");
            wp.setProperty("smtpUseCreds", scnr.nextLine());

            if (wp.getProperty("smtpUseCreds").equals("true")) {
                System.out.print("Your smtp username (has to be able to write emails):");
                wp.setProperty("smtpUserName", scnr.nextLine());
                System.out.println("Your smtp password:");
                wp.setProperty("smtpPassword", scnr.nextLine());
            } else {
                wp.setProperty("smtpUserName", "");
                wp.setProperty("smtpPassword", "");
            }
            System.out.println("Path to your users db (e.g. jdbc:mysql://172.17.0.3:3306/users):");
            wp.setProperty("dbPath", scnr.nextLine());
            System.out.println("The user you want to use on that db:");
            wp.setProperty("dbUser", scnr.nextLine());
            System.out.println("The user's password:");
            wp.setProperty("dbPassword", scnr.nextLine());

            try {
                wp.store(os, null); //saves properties to file
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        clearConsole();
        System.out.println("Finished setup!");
    }

    public static void clearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }

}
