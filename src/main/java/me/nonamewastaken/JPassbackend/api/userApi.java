package me.nonamewastaken.JPassbackend.api;

import me.nonamewastaken.JPassbackend.dbEditor;
import me.nonamewastaken.JPassbackend.emailSender;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
public class userApi {
    emailSender sender = new emailSender();

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/api/addUser/{email}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public String addUserToEmailValidation(@PathVariable String email){
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        sender = new emailSender();
        sender.connect("localhost", "1025", "", "", false); //local mailhog testing server //TODO make use of settingsManager()
        try {
            sender.send("email@email.com", "JPass", email, "JPass email verification", "Verification code: "+n); //sends email with verification code
        } catch (Exception e) {
            e.printStackTrace();
        }
        String returnString = dbEditor.addEmailCodeToDB(email, String.valueOf(n)); //adds user and returns return from addUser() function to client (success! / Internal server error!/ user already exists!)
        System.out.println(returnString);
        if(returnString.equals("resend code")){ //resend code if user already in db
            try {
                sender.send("email@email.com", "JPass", email, "JPass email verification", "Verification code: "+dbEditor.getEmailCode(email)); //sends email with verification code
                return "resent email";
            } catch (Exception e) {
                e.printStackTrace();
                return "could not resend email";
            }
        }else{
        return returnString;
        }
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/api/addUserVerified/{name}/{key}/{verifyCode}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public String addUser(@PathVariable String name, @PathVariable String key, @PathVariable String verifyCode){
        return dbEditor.addUser(name, key, verifyCode); //adds user and returns return from addUser() function to client (success! / Internal server error!/ user already exists!)
    }

    @RequestMapping(
            method = RequestMethod.POST,
            path = "/api/addDataToUser/{name}/{key}/{data}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public String addData(@PathVariable String name, @PathVariable String key, @RequestBody String data){
        return dbEditor.addDataToUser(name, data, key);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/error",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public String error(){
        return "404, PAGE NOT FOUND!";
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/api/getData/{name}/{key}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public String getData(@PathVariable String name, @PathVariable String key){
        return dbEditor.fetchData(name, key); //adds user and returns return from addUser() function to client (success! / Internal server error!/ user already exists!)
    }
}
