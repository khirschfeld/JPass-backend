package me.nonamewastaken.JPassbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
public class JPassBackendApplication {

	static public String table = "users";
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		settingsManager sm = new settingsManager();

		try {
			sm.readSettings();
		} catch (IOException e) {
			//run first startup script
			System.out.println("You seem to have opened this program for the first time, do you want to run the first setup script or configure everything manually?\n1)run startup helper \n2)I'll figure it out");
			if(scanner.nextInt() == 1){
				sm.setup();
				//run startup
			}
		}

		SpringApplication.run(JPassBackendApplication.class, args);
		String dbPath = sm.rp.getProperty("dbPath").replaceAll("\\\\\\\\", "");// replaces \es in string to combat weird \es added by java
		dbEditor.establishConnection(dbPath, sm.rp.getProperty("dbUser"), sm.rp.getProperty("dbPassword")); //connects to db with data from .config file
		emailSender sender = new emailSender();
		sender.connect(sm.rp.getProperty("smtpHost"), sm.rp.getProperty("smtpPort"), sm.rp.getProperty("smtpUserName"), sm.rp.getProperty("smtpPassword"), Boolean.parseBoolean(sm.rp.getProperty("smtpUseCreds"))); //connects to mail server with data from .config file

	}
}
